bidApp
.controller('MainCtrl', function($scope) {

	$scope.showing_status = {
		loggined : false,
		is_bubble: false
	};

	$scope.log_message = [];

	$scope.signIn = function(){

		$scope.showing_status.loggined = true;
	};

	$scope.signOut = function(){

		$scope.showing_status.loggined = false;
	};

	$scope.bubble_click_open = function(){

		$scope.showing_status.is_bubble = true;
	};

	$scope.bubble_click_close = function(){

		$scope.log_message = [];
		$scope.showing_status.is_bubble = false;
	};
})
