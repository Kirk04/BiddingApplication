
var bidApp = angular.module('bidApp', ['ionic'])

bidApp.run(function($ionicPlatform) {

    $ionicPlatform.ready(function() {

        if (window.cordova && window.cordova.plugins.Keyboard) {

            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {

            StatusBar.styleDefault();
        }
    });
})

bidApp.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

    .state('bidiginter', {
        url: '/bidiginter',
        abstract: true,
        templateUrl: 'pages/SideMenu.html',
        controller: 'MainCtrl'
    })

    .state('bidiginter.mainpage', {
        url: '/mainpage',
            views: {
            'menuContent': {
                templateUrl: 'pages/MainPage.html',
                controller: 'MainCtrl'
            }
        }
    })

    // .state('bidiginter.single', {
    //     url: '/playlists/:playlistId',
    //         views: {
    //         'menuContent': {
    //             templateUrl: 'templates/playlist.html',
    //             controller: 'MainCtrl'
    //         }
    //     }
    // });

    $urlRouterProvider.otherwise('/bidiginter/mainpage');
});
